package ru.inordic.arj.gradle.springbootproject

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootProjectApplication

fun main(args: Array<String>) {
	runApplication<SpringBootProjectApplication>(*args)
}
